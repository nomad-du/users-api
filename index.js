const app = require('express')();
const bodyParser = require('body-parser');
const dotenv = require('dotenv');

dotenv.config();

const PGUSER = (() => {
  if (!process.env.PGUSER) {
    throw new Error('Need PGUSER env var');
  }

  return process.env.PGUSER;
})();

const PGPASSWORD = (() => {
  if (!process.env.PGPASSWORD) {
    throw new Error('Need PGPASSWORD env var');
  }

  return process.env.PGPASSWORD;
})();

const PGHOST = (() => {
  if (!process.env.PGHOST) {
    throw new Error('Need PGHOST env var');
  }

  return process.env.PGHOST;
})();

const PGPORT = (() => {
  if (!process.env.PGPORT) {
    throw new Error('Need PGPORT env var');
  }

  return process.env.PGPORT;
})();

const PGDATABASE = (() => {
  if (!process.env.PGDATABASE) {
    throw new Error('Need PGDATABASE env var');
  }

  return process.env.PGDATABASE;
})();

const PORT = process.env.PORT || 5000;

const createUsersClient = require('./src/user');

app.use(bodyParser.json());

const run = async () => {
  try {
    const users = await createUsersClient({
      host:     PGHOST,
      port:     PGPORT,
      user:     PGUSER,
      password: PGPASSWORD,
      database: PGDATABASE,
    });

    app.get('/users', async (req, res) => {
      try {
        const result = await users.getAll();
        res.status(200).json({ users: result });
      } catch(e) {
        console.error(e);
        res.sendStatus(500);
      }

    });

    app.post('/users/:userId', async ({ params, body }, res) => {
      try {
        const { userId } = params;
        const { user } = body;

        console.log('received user', user, 'for id', userId);

        await users.save(userId, user);
        res.sendStatus(204);
      } catch(e) {
        console.error(e);
        res.sendStatus(500);
      }
    });

    app.get('/users/:userId', async ({ params }, res) => {
      try {
        const { userId } = params;
        const user = await users.get(userId);

        if (user) {
          res.status(200).json({ user });
        } else {
          res.sendStatus(200);
        }
      } catch(e) {
        console.error(e);
        res.sendStatus(500);
      }
    });

    app.get('/users-ics', async ({ params }, res) => {
      try {
        const ics = await users.getAllIcs();

        res.json({ ics });
      } catch(e) {
        console.error(e);
        res.sendStatus(500);
      }
    });
  } catch(e) {
    console.error(e);
    process.exit(-1);
  }

};

run();

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
