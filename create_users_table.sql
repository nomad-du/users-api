CREATE TABLE users (
  id            TEXT PRIMARY KEY,
  name          TEXT,
  img           TEXT,
  team          TEXT,

  integrations  JSONB,

  options       JSONB,

  created_at    TIMESTAMP DEFAULT now(),
  updated_at    TIMESTAMP DEFAULT now()
)

