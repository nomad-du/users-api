const { Client } = require('pg');

const createUsersClient = async (config) => {
  const client = new Client(config);
  try {
    await client.connect();
  } catch(e) {
    console.error(e);
    process.exit(-1);
  }

  return {
    save: async (userId, user) => {
      const query = `
        INSERT INTO users (
          id,
          name,
          img,
          team,
          integrations,
          options,
          updated_at
        )
        VALUES (
          $1,
          $2,
          $3,
          $4,
          $5::jsonb,
          $6::jsonb,
          $7
        )
        ON CONFLICT (id) DO UPDATE
          SET
            name = $2,
            img = $3,
            team = $4,
            integrations = $5::jsonb,
            options = $6::jsonb,
            updated_at = $7;
      `;

      const values = [
        userId,
        user.name,
        user.img,
        user.team,
        JSON.stringify(user.integrations),
        JSON.stringify(user.options),
        new Date(),
      ];

      return await client.query(query, values);
    },
    getAll: async (userId) => {
      const query = 'SELECT * FROM users';

      const res = await client.query(query);

      return res.rows ? res.rows : [];
    },
    get: async (userId) => {
      const query = `SELECT * FROM users WHERE id = '${userId}'`;

      const res = await client.query(query);

      return res.rows ? res.rows[0] : null;
    },
    getAllIcs: async () => {
      const query = `
        SELECT
          id as user_id,
          jsonb_array_elements(integrations->'ics') as url
        FROM users;
      `;

      const res = await client.query(query);
      return res.rows;
    }
  };
};

module.exports = createUsersClient;
